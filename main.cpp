#include <SFML/Graphics.hpp>
#include <X11/Xlib.h>
#include <thread>
#include <iostream>

#include "draw_wrapper.h"
#include "main_logic.h"

int main(int argc, char **argv)
{

#ifdef NotWin
    if (!XInitThreads())
    {
        std::cerr << "XInitThreads failed" << std::endl;
        return -1;
    }
#endif // NotWin  
      
    std::srand(std::time(0));

    sf::RenderWindow window(sf::VideoMode(900, 900), "Game", sf::Style::Close);

    window.setActive(false);

    draw_wrapper draw(&window);

    sf::Thread thread_draw([&draw]()
                           { draw.draw(); });

    thread_draw.launch();

    main_logic logic(draw);

    logic.init();

    sf::Thread thread_logic([&logic]()
                            { logic.start(); });

    thread_logic.launch();
    //
    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
            {
                logic.close();
                draw.close();
                thread_logic.wait();
                thread_draw.wait();
                window.close();
                return 0;
            }
            else if (event.type == sf::Event::MouseButtonPressed)
            {
                if (event.mouseButton.button == sf::Mouse::Button::Left)
                {
                    logic.mouse_click(event.mouseButton.x, event.mouseButton.y);
                }
            }
        }
        std::this_thread::sleep_for(std::chrono::microseconds(500));
    }
    return 0;
}
