#ifndef MAIN_DATA_H_
#define MAIN_DATA_H_

#include <SFML/System/Vector2.hpp>
#include <SFML/Graphics/Texture.hpp>

namespace main_data
{
    const static int CELL_HEIGHT = 110;
    const static int CELL_WIDTH = 110;

    struct lazy_texture
    {
        sf::Texture texture;
        bool loaded;
    };

    static sf::Vector2f calk_position(int x, int y)
    {
        return sf::Vector2f(x * CELL_WIDTH + 10, y * CELL_HEIGHT + 10);
    }

    static sf::Vector2i get_board_coord()
    {
        return sf::Vector2i(10, 10);
    }

    static sf::Vector2i calk_cell_from_position(int x_pos, int y_pos)
    {
        //
        int x = (x_pos - get_board_coord().x) / CELL_WIDTH;
        int y = (y_pos - get_board_coord().y) / CELL_HEIGHT;
        return {x, y};
        //
    }
};

#endif // !MAIN_DATA_H_
