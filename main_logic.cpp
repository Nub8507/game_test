#include "main_logic.h"

#include <thread>

main_logic::main_logic(draw_wrapper &draw)
    : draw(draw), ai_class(this)
{
    //
    enabled.store(true);
    player_turn = main_logic::playerColor::White;
    human_player = main_logic::playerColor::White;
    //human_player = main_logic::playerColor::NoColor;
    selected_chip.x = -1;
    start_ai_think = false;
    game_finish.store(false);
    //
}

void main_logic::init()
{
    //
    black = {{0, 0}, {1, 0}, {2, 0}, {0, 1}, {1, 1}, {2, 1}, {0, 2}, {1, 2}, {2, 2}};
    white = {{5, 7}, {6, 7}, {7, 7}, {5, 6}, {6, 6}, {7, 6}, {5, 5}, {6, 5}, {7, 5}};
    //
    draw.clear_chips();
    //
    for (int i = 1; i <= black.size(); ++i)
    {
        draw.set_chip(i, draw_wrapper::chipColor::Black, black[i - 1].x, black[i - 1].y);
    }
    //
    for (int i = 1; i <= white.size(); ++i)
    {
        draw.set_chip(i + 10, draw_wrapper::chipColor::White, white[i - 1].x, white[i - 1].y);
    }
    //
    black_target = {7, 7};
    white_target = {0, 0};
    //
}

void main_logic::start()
{
    //
    while (enabled.load(std::memory_order_relaxed))
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(1500));
        if (player_turn != human_player && !start_ai_think)
        {
            start_ai_think = true;
            std::lock_guard<std::recursive_mutex> lock(mut_chips);
            if (player_turn == main_logic::playerColor::Black)
            {
                ai_class.do_next_move(black, white, {7, 7});
            }
            else if (player_turn == main_logic::playerColor::White)
            {
                ai_class.do_next_move(white, black, {0, 0});
            }
        }
    }
    //
}

void main_logic::close()
{
    enabled.store(false, std::memory_order_relaxed);
}

void main_logic::mouse_click(int x_pos, int y_pos)
{
    auto cell = main_data::calk_cell_from_position(x_pos, y_pos);
    std::lock_guard<std::recursive_mutex> lock(mut_chips);
    //
    auto color = chip_color(cell);
    if (human_player == player_turn)
    {
        if (color == player_turn)
        {
            if (selected_chip == cell)
            {
                selected_chip.x = -1;
                draw.select_chip(-1, -1);
                change_turn();
            }
            else
            {
                selected_chip = cell;
                draw.select_chip(cell.x, cell.y);
            }
        }
        else if (color == main_logic::playerColor::Empty)
        {
            if (selected_chip.x != -1)
            {
                auto delta = selected_chip - cell;
                if (delta.x * delta.x + delta.y * delta.y == 1)
                {
                    move_chip(selected_chip, cell);
                    selected_chip.x = -1;
                    draw.select_chip(-1, -1);
                    change_turn();
                }
            }
        }
    }
    //
}

main_logic::playerColor main_logic::chip_color(sf::Vector2i pos)
{
    //
    std::lock_guard<std::recursive_mutex> lock(mut_chips);
    //
    for (auto &chip : black)
    {
        if (chip == pos)
        {
            return main_logic::playerColor::Black;
        }
    }
    for (auto &chip : white)
    {
        if (chip == pos)
        {
            return main_logic::playerColor::White;
        }
    }
    //
    return main_logic::playerColor::Empty;
    //
}

void main_logic::move_chip(sf::Vector2i src, sf::Vector2i dst)
{
    //
    if (game_finish.load(std::memory_order_relaxed))
    {
        return;
    }
    //
    std::lock_guard<std::recursive_mutex> lock(mut_chips);
    //
    for (auto &chip : black)
    {
        if (chip == src)
        {
            draw.move_chip(src.x, src.y, dst.x, dst.y);
            chip = dst;
            return;
        }
    }
    for (auto &chip : white)
    {
        if (chip == src)
        {
            draw.move_chip(src.x, src.y, dst.x, dst.y);
            chip = dst;
            return;
        }
    }
    //
}

void main_logic::ai_move_chip(sf::Vector2i src, sf::Vector2i dst)
{
    //
    if (game_finish.load(std::memory_order_relaxed))
    {
        return;
    }
    //
    move_chip(src, dst);
    start_ai_think = false;
    change_turn();
}

void main_logic::change_turn()
{
    //
    auto win = check_win();
    if (win == main_logic::playerColor::Black)
    {
        draw.show_win("Black win!");
    }
    else if (win == main_logic::playerColor::White)
    {
        draw.show_win("White win!");
    }
    //
    if (player_turn == main_logic::playerColor::Black)
    {
        player_turn = main_logic::playerColor::White;
    }
    else if (player_turn == main_logic::playerColor::White)
    {
        player_turn = main_logic::playerColor::Black;
    }
    //
}

main_logic::playerColor main_logic::check_win()
{
    //
    std::lock_guard<std::recursive_mutex> lock(mut_chips);
    const std::vector<sf::Vector2i> base_black = {{0, 0}, {1, 0}, {2, 0}, {0, 1}, {1, 1}, {2, 1}, {0, 2}, {1, 2}, {2, 2}};
    const std::vector<sf::Vector2i> base_white = {{5, 7}, {6, 7}, {7, 7}, {5, 6}, {6, 6}, {7, 6}, {5, 5}, {6, 5}, {7, 5}};
    //
    int sum = 0;
    for (const auto &chip : black)
    {
        if (std::find(base_white.begin(), base_white.end(), chip) != base_white.end())
        {
            sum++;
        }
    }
    //
    if (sum == 9)
    {
        game_finish.store(true, std::memory_order_relaxed);
        return main_logic::playerColor::Black;
    }
    //
    sum = 0;
    for (const auto &chip : white)
    {
        if (std::find(base_black.begin(), base_black.end(), chip) != base_black.end())
        {
            sum++;
        }
    }
    //
    if (sum == 9)
    {
        game_finish.store(true, std::memory_order_relaxed);
        return main_logic::playerColor::White;
    }
    //
    return main_logic::playerColor::NoColor;
    //
}
