#ifndef DRAW_WRAPPER_H_
#define DRAW_WRAPPER_H_

#include <cstdint>
#include <atomic>
#include <list>
#include <mutex>

#include "main_data.h"

namespace sf
{
    class RenderWindow;
    class Sprite;
    class Texture;
}

class draw_wrapper
{

public:
    enum class chipColor
    {
        Black,
        White,
    };

    draw_wrapper(sf::RenderWindow *window = nullptr);
    virtual ~draw_wrapper() = default;

    void draw();
    void close();

    void set_chip(int id, chipColor color, int x_pos, int y_pos);
    void move_chip(int id, int new_x_pos, int new_y_pos);
    void move_chip(int x_pos, int y_pos, int new_x_pos, int new_y_pos);
    void select_chip(int x_pos, int y_pos);
    void clear_chips();
    void show_win(std::string str);

private:
    struct chip
    {
        int id;
        chipColor color;
        int x_pos;
        int y_pos;
        int new_x_pos;
        int new_y_pos;
        main_data::lazy_texture texture;
        sf::Sprite *sprite;
        bool moving;

        chip(int id, chipColor color, int x_pos, int y_pos);
        ~chip();
    };

    struct selection{
        sf::Vector2i pos;
        sf::Color color;
    };

    std::atomic<bool> enabled;
    std::list<chip> chips;
    std::mutex mut_chips;
    std::mutex mut_win;
    std::string win_string;
    std::vector<selection> selected;

    sf::RenderWindow *window;

    void timer_draw();
    void draw_board();
    void draw_chips();
    void draw_selected();
    void draw_win_string();
    void do_move_chip(draw_wrapper::chip &chip);
};

#endif // !DRAW_WRAPPER_H_
