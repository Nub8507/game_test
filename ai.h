#ifndef AI_H_
#define AI_H_

#include <mutex>
#include <vector>

#include <SFML/System/Vector2.hpp>

class main_logic;

class ai
{

public:
    ai(main_logic *logic);
    virtual ~ai() = default;

    void do_next_move(const std::vector<sf::Vector2i> &my,
                      const std::vector<sf::Vector2i> &enemy,
                      const sf::Vector2i target);

private:
    main_logic *logic;
    std::mutex mut_calk;

    sf::Vector2i best_src, best_dst;
    float best_change;

    bool contain(const std::vector<sf::Vector2i> &vector,
                 const sf::Vector2i point);

    void calk_chip_move(const std::vector<sf::Vector2i> &my,
                        const std::vector<sf::Vector2i> &enemy,
                        const sf::Vector2i chip,
                        const sf::Vector2i target);

    float calk_dist(const sf::Vector2i src,const sf::Vector2i dst);
};

#endif // !AI_H_
