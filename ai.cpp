#include "ai.h"
#include "main_logic.h"

#include <cmath>

ai::ai(main_logic *logic)
{
    this->logic = logic;
}

void ai::do_next_move(const std::vector<sf::Vector2i> &my,
                      const std::vector<sf::Vector2i> &enemy,
                      const sf::Vector2i target)
{
    std::lock_guard<std::mutex> lock(mut_calk);
    best_src = {-1, -1};
    best_dst = {-1, -1};
    best_change = -20;
    //
    for (const auto &chip : my)
    {
        calk_chip_move(my, enemy, chip, target);
    }
    //
    logic->ai_move_chip(best_src, best_dst);
}

bool ai::contain(const std::vector<sf::Vector2i> &vector,
                 const sf::Vector2i point)
{
    //
    for (const auto &v : vector)
    {
        if (v == point)
            return true;
    }
    //
    return false;
    //
}

void ai::calk_chip_move(const std::vector<sf::Vector2i> &my,
                        const std::vector<sf::Vector2i> &enemy,
                        const sf::Vector2i chip,
                        const sf::Vector2i target)
{
    //
    auto chip_dist = calk_dist(chip, target);
    auto func = [this, &target, &my, &enemy, &chip_dist, &chip](sf::Vector2i new_pos)
    {
        if (!contain(my, new_pos) && !contain(enemy, new_pos))
        {
            if(new_pos==target){
                    this->best_change = 100;
                    this->best_src = chip;
                    this->best_dst = new_pos;                
            }
            auto new_pos_dist = calk_dist(new_pos, target);
            if (new_pos_dist != 0)
            {
                auto dist = (chip_dist - new_pos_dist);
                if (dist > 0)
                {
                    dist *= chip_dist;
                }
                else
                {
                    dist /= chip_dist;
                }

                if (this->best_change < dist)
                {
                    this->best_change = dist;
                    this->best_src = chip;
                    this->best_dst = new_pos;
                }
            }
        }
    };
    sf::Vector2i new_pos = chip;
    new_pos.x--;
    func(new_pos);
    //
    new_pos = chip;
    new_pos.x++;
    func(new_pos);
    //
    new_pos = chip;
    new_pos.y--;
    func(new_pos);
    //
    new_pos = chip;
    new_pos.y++;
    func(new_pos);
    //
}

float ai::calk_dist(const sf::Vector2i src, const sf::Vector2i dst)
{
    //
    if (src.x < 0 || src.y < 0 || src.x > 7 || src.y > 7)
    {
        return 0;
    }
    //
    float len_x = src.x - dst.x;
    float len_y = src.y - dst.y;
    //
    return std::sqrt(len_x * len_x + len_y * len_y);
    //
}