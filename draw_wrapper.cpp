#include "draw_wrapper.h"

#include <SFML/Graphics.hpp>
#include <chrono>
#include <thread>

#include "main_data.h"

draw_wrapper::draw_wrapper(sf::RenderWindow *window) : window(window)
{
    if (this->window != nullptr)
        enabled.store(true);
    else
    {
        enabled.store(false);
    }
    chips.clear();
    selected.clear();
    win_string = "";
}

void draw_wrapper::close()
{
    enabled.store(false, std::memory_order_relaxed);
}

void draw_wrapper::draw()
{
    //
    window->setActive(true);
    //
    const auto freq_time = std::chrono::milliseconds(20);
    auto start_time = std::chrono::steady_clock::now();
    while (enabled.load(std::memory_order_relaxed))
    {
        timer_draw();
        auto end_time = std::chrono::steady_clock::now();
        auto interval = std::chrono::duration_cast<std::chrono::microseconds>(freq_time - (end_time - start_time)).count();
        start_time = std::chrono::steady_clock::now();
        if (interval > 0)
            std::this_thread::sleep_for(std::chrono::microseconds(interval));
    }
    //
}

void draw_wrapper::timer_draw()
{

    if (window->isOpen())
    {
        window->clear(sf::Color::Yellow);

        draw_board();
        draw_selected();
        draw_chips();
        draw_win_string();

        window->display();
    }
}

void draw_wrapper::draw_board()
{
    //
    sf::RectangleShape rectangle_white;
    sf::RectangleShape rectangle_black;

    rectangle_white.setSize(sf::Vector2f(main_data::CELL_HEIGHT, main_data::CELL_WIDTH));
    rectangle_white.setFillColor(sf::Color::White);
    rectangle_black.setSize(sf::Vector2f(main_data::CELL_HEIGHT, main_data::CELL_WIDTH));
    rectangle_black.setFillColor(sf::Color::Black);

    for (auto i = 0; i < 8; ++i)
    {
        for (auto j = 0; j < 8; ++j)
        {
            if ((i + j) % 2)
            {
                rectangle_white.setPosition(main_data::calk_position(i, j));
                window->draw(rectangle_white);
            }
            else
            {
                rectangle_black.setPosition(main_data::calk_position(i, j));
                window->draw(rectangle_black);
            }
        }
    }
    //
}

void draw_wrapper::draw_chips()
{
    std::lock_guard<std::mutex> lock(mut_chips);
    for (auto &chip : chips)
    {
        if (chip.sprite != nullptr)
        {
            if (chip.moving == false)
            {
                window->draw(*chip.sprite);
            }
            else
            {
                do_move_chip(chip);
            }
        }
    }
}

draw_wrapper::chip::chip(int id, chipColor color, int x_pos, int y_pos)
{
    this->id = id;
    this->color = color;
    this->x_pos = x_pos;
    this->y_pos = y_pos;
    this->new_x_pos = x_pos;
    this->new_y_pos = y_pos;
    this->moving = false;
    this->sprite = nullptr;
    if (color == draw_wrapper::chipColor::Black)
    {
        if (texture.texture.loadFromFile("black.png"))
        {
            texture.loaded = true;
        }
        else
        {
            texture.loaded = false;
        }
    }
    else if (color == draw_wrapper::chipColor::White)
    {
        if (texture.texture.loadFromFile("white.png"))
        {
            texture.loaded = true;
        }
        else
        {
            texture.loaded = false;
        }
    }
    if (texture.loaded)
    {
        this->sprite = new sf::Sprite(texture.texture);
        this->sprite->setPosition(main_data::calk_position(x_pos, y_pos));
    }
}

draw_wrapper::chip::~chip()
{
    if (this->sprite != nullptr)
    {
        delete this->sprite;
        this->sprite = nullptr;
    }
}

void draw_wrapper::set_chip(int id, chipColor color, int x_pos, int y_pos)
{
    //
    std::lock_guard<std::mutex> lock(mut_chips);
    chips.emplace_back(id, color, x_pos, y_pos);
    //
}

void draw_wrapper::move_chip(int id, int new_x_pos, int new_y_pos)
{
    //
    std::lock_guard<std::mutex> lock(mut_chips);
    for (auto &chip : chips)
    {
        if (chip.id == id)
        {
            chip.moving = true;
            chip.new_x_pos = new_x_pos;
            chip.new_y_pos = new_y_pos;
        }
    }
    //
}

void draw_wrapper::move_chip(int x_pos, int y_pos, int new_x_pos, int new_y_pos)
{
    //
    std::lock_guard<std::mutex> lock(mut_chips);
    for (auto &chip : chips)
    {
        if (chip.x_pos == x_pos && chip.y_pos == y_pos)
        {
            chip.moving = true;
            chip.new_x_pos = new_x_pos;
            chip.new_y_pos = new_y_pos;
        }
    }
    //
}

void draw_wrapper::do_move_chip(draw_wrapper::chip &chip)
{
    //
    if (chip.new_x_pos > chip.x_pos)
    {
        chip.sprite->move(1, 0);
    }
    else if (chip.new_x_pos < chip.x_pos)
    {
        chip.sprite->move(-1, 0);
    }
    if (chip.new_y_pos > chip.y_pos)
    {
        chip.sprite->move(0, 1);
    }
    else if (chip.new_y_pos < chip.y_pos)
    {
        chip.sprite->move(0, -1);
    }
    //
    window->draw(*chip.sprite);
    //
    auto new_pos = main_data::calk_position(chip.new_x_pos, chip.new_y_pos);
    auto pos = chip.sprite->getPosition();
    auto np = new_pos - pos;
    if (np.x * np.x + np.y * np.y < 9)
    {
        chip.moving = false;
        chip.x_pos = chip.new_x_pos;
        chip.y_pos = chip.new_y_pos;
    }
    //
}

void draw_wrapper::select_chip(int x_pos, int y_pos)
{
    //
    std::lock_guard<std::mutex> lock(mut_chips);
    //
    selected.clear();
    if (x_pos == -1)
    {
        return;
    }
    //
    selection sel = {{x_pos - 1, y_pos}, sf::Color(0, 255, 0, 90)};
    selected.push_back(sel);
    sel = {{x_pos, y_pos - 1}, sf::Color(0, 255, 0, 90)};
    selected.push_back(sel);
    sel = {{x_pos + 1, y_pos}, sf::Color(0, 255, 0, 90)};
    selected.push_back(sel);
    sel = {{x_pos, y_pos + 1}, sf::Color(0, 255, 0, 90)};
    selected.push_back(sel);
    //
    for (auto &chip : chips)
    {
        if (chip.new_x_pos == x_pos - 1 && chip.new_y_pos == y_pos)
        {
            selected[0].color = sf::Color(255, 0, 0, 90);
        }
        else if (chip.new_x_pos == x_pos && chip.new_y_pos == y_pos - 1)
        {
            selected[1].color = sf::Color(255, 0, 0, 90);
        }
        else if (chip.new_x_pos == x_pos + 1 && chip.new_y_pos == y_pos)
        {
            selected[2].color = sf::Color(255, 0, 0, 90);
        }
        else if (chip.new_x_pos == x_pos && chip.new_y_pos == y_pos + 1)
        {
            selected[3].color = sf::Color(255, 0, 0, 90);
        }
    }
    //
}

void draw_wrapper::show_win(std::string str)
{
    //
    std::lock_guard<std::mutex> lock(mut_win);
    win_string = str;
    //
}

void draw_wrapper::clear_chips()
{
    //
    std::lock_guard<std::mutex> lock(mut_chips);
    chips.clear();
    //
}

void draw_wrapper::draw_win_string()
{
    //
    sf::Font font;
    if (!font.loadFromFile("roboto.ttf"))
        return;
    std::lock_guard<std::mutex> lock(mut_win);
    if (win_string.length() > 0)
    {
        sf::Text text(win_string, font);
        text.setCharacterSize(160);
        text.setPosition(70, window->getSize().y / 2 - 160 / 2);
        text.setStyle(sf::Text::Bold);
        text.setFillColor(sf::Color::Red);
        window->draw(text);
    }
    //
}

void draw_wrapper::draw_selected()
{
    //
    std::lock_guard<std::mutex> lock(mut_chips);
    sf::RectangleShape rectangle;
    rectangle.setSize(sf::Vector2f(main_data::CELL_HEIGHT, main_data::CELL_WIDTH));

    for (auto &chip : selected)
    {
        if (chip.pos.x >= 0 && chip.pos.x < 9 && chip.pos.y >= 0 && chip.pos.y < 9)
        {
            rectangle.setFillColor(chip.color);
            rectangle.setPosition(main_data::calk_position(chip.pos.x, chip.pos.y));
            window->draw(rectangle);           
        }
    }
    //
}
