#ifndef MAIN_LOGIC_H_
#define MAIN_LOGIC_H_

#include <cstdint>
#include <atomic>

#include "draw_wrapper.h"
#include "main_data.h"
#include "ai.h"

class main_logic
{

public:
    main_logic(draw_wrapper &draw);
    virtual ~main_logic() = default;

    void init();
    void start();
    void close();
    void mouse_click(int x_pos, int y_pos);
    void ai_move_chip(sf::Vector2i src, sf::Vector2i dst);

private:
    enum class playerColor
    {
        Black,
        White,
        Empty,
        NoColor,
    };

    draw_wrapper &draw;
    ai ai_class;
    std::atomic<bool> enabled;
    std::recursive_mutex mut_chips;
    std::atomic<bool> game_finish;

    std::vector<sf::Vector2i> black;
    std::vector<sf::Vector2i> white;
    sf::Vector2i black_target;
    sf::Vector2i white_target;
    sf::Vector2i selected_chip;
    playerColor player_turn;
    playerColor human_player;
    bool start_ai_think;

    playerColor chip_color(sf::Vector2i pos);
    void move_chip(sf::Vector2i src, sf::Vector2i dst);
    void change_turn();
    playerColor check_win();
};

#endif // !MAIN_LOGIC_H_
